import React from 'react'
import { Image, Text } from 'react-native'
import { createAppContainer, createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';

import ListFood from './pages/main/List';
import DetailsFood from './pages/food/Details';
import DetailsProduct from './pages/details';

import logo from './assets/logo.png';

const tabFooter = createBottomTabNavigator({
    ListFood: {
        screen: ListFood,
        navigationOptions: {
            tabBarLabel: 'ListFood',
            tabBarIcon: ({ focused, tintColor }) => {
                return <Icon name="event" size={25} color={tintColor} />;
            }
        },
    },
    // DetailsFood: {
    //     screen: DetailsFood,
    //     navigationOptions: {
    //         tabBarLabel: 'DetailsFood',
    //         tabBarIcon: ({ focused, tintColor }) => {
    //             return <Icon name="event" size={25} color={tintColor} />;
    //         }
    //     },
    // }
}, {
    tabBarOptions: {
        style: { backgroundColor: '#040B11' }
    },
    initialRouteName: "ListFood"
})

const Principal = createStackNavigator({
    Inicial: {
        screen: tabFooter,
        navigationOptions: (props) => ({
            headerStyle: {
                backgroundColor: '#f2f6f9'
            },
            headerTitle: <Text>Titulo</Text>,
            // headerTitle: <Image style={{ marginLeft: 10, width: 150, height: 25 }} source={logo} />,
        })
    },
    DetailsProduct: {
        screen: DetailsProduct,
        navigationOptions: (props) => ({
            headerStyle: {
                backgroundColor: '#f2f6f9'
            },
            headerTitle: <Text>Descricao</Text>
        })
    }
}, { initialRouteName: "Inicial" })



export default createAppContainer(Principal)
