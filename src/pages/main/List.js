import React, { useState, useEffect } from 'react';
import { Text, ScrollView, Image, ActivityIndicator, TouchableOpacity, FlatList, View } from 'react-native';
import Reactotron from 'reactotron-react-native';

import styles from './styles';
import api from '../../services';

const List = (props) => {

    const [loadingDestaque, setLoadingDestaque] = useState(true)
    const [totalRegistrosDestaque, setTotalRegistrosDestaque] = useState(0)
    const [paginaDestaque, setPaginaDestaque] = useState(0)
    const [registrosDestaque, setRegistrosDestaque] = useState([])

    const [loadingPromocao, setLoadingPromocao] = useState(true)
    const [totalRegistrosPromocao, setTotalRegistrosPromocao] = useState(0)
    const [paginaPromocao, setPaginaPromocao] = useState(0)
    const [registrosPromocao, setRegistrosPromocao] = useState([])

    const [loadingFantasia, setLoadingFantasia] = useState(true)
    const [totalRegistrosFantasia, setTotalRegistrosFantasia] = useState(0)
    const [paginaFantasia, setPaginaFantasia] = useState(0)
    const [registrosFantasia, setRegistrosFantasia] = useState([])

    useEffect(() => {
        api.get('/food').then((res) => {
            setRegistrosDestaque(res.data.registros)
            setTotalRegistrosDestaque(res.data.total)
            setLoadingDestaque(false)
        })

        api.get('/food').then((res) => {
            setRegistrosPromocao(res.data.registros)
            setTotalRegistrosPromocao(res.data.total)
            setLoadingPromocao(false)
        })

        api.get('/food').then((res) => {
            setRegistrosFantasia(res.data.registros)
            setTotalRegistrosFantasia(res.data.total)
            setLoadingFantasia(false)
        })
    }, [])

    renderItem = (item, index) => {
        return (
            <View key={index} style={styles.card}>
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailsProduct', { _id: item._id })}>
                    <Text style={styles.titulo} >{item.title}</Text>
                    <Image style={styles.foto} source={{ uri: 'https://picsum.photos/id/' + index + '/300/300' }} />
                </TouchableOpacity>
            </View>
        )
    }

    onEndReachedDestaque = (item) => {
        if ((paginaDestaque * registrosDestaque.length) < totalRegistrosDestaque) {
            setLoadingDestaque(true)
            setPaginaDestaque(paginaDestaque + 1)

            setTimeout(() => {
                api.get('/food?page=' + (paginaDestaque + 1)).then((res) => {
                    setRegistrosDestaque([...registrosDestaque, ...res.data.registros])
                    setLoadingDestaque(false)
                })
            }, 2000)
        }
    }

    onEndReachedPromocao = (item) => {
        if ((paginaPromocao * registrosPromocao.length) < totalRegistrosPromocao) {
            setLoadingPromocao(true)
            setPaginaPromocao(paginaPromocao + 1)

            setTimeout(() => {
                api.get('/food?page=' + (paginaPromocao + 1)).then((res) => {
                    setRegistrosPromocao([...registrosPromocao, ...res.data.registros])
                    setLoadingPromocao(false)
                })
            }, 2000)
        }
    }

    onEndReachedFantasia = (item) => {
        if ((paginaFantasia * registrosFantasia.length) < totalRegistrosFantasia) {
            setLoadingFantasia(true)
            setPaginaFantasia(paginaFantasia + 1)

            setTimeout(() => {
                api.get('/food?page=' + (paginaFantasia + 1)).then((res) => {
                    setRegistrosFantasia([...registrosFantasia, ...res.data.registros])
                    setLoadingFantasia(false)
                })
            }, 2000)
        }
    }

    renderFooterDestaque = () => {
        if (loadingDestaque) {
            return (
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                    <ActivityIndicator animating size="large" color="#0000ff" />
                </View>
            )
        }

        return null;
    }

    renderFooterPromocao = () => {
        if (loadingPromocao) {
            return (
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                    <ActivityIndicator animating size="large" color="#0000ff" />
                </View>
            )
        }

        return null;
    }

    renderFooterFantasia = () => {
        if (loadingFantasia) {
            return (
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                    <ActivityIndicator animating size="large" color="#0000ff" />
                </View>
            )
        }

        return null;
    }

    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={[styles.cardsContainer, { backgroundColor: '#F5F5F5' }]}>
                    <Text>Em destaque</Text>
                    <FlatList data={registrosDestaque} horizontal={true} renderItem={({ item, index }) => this.renderItem(item, index)} keyExtractor={(item) => item._id.toString()} onEndReached={onEndReachedDestaque} onEndReachedThreshold={0.5} ListFooterComponent={renderFooterDestaque} />
                </View>

                <View style={[styles.cardsContainer, { backgroundColor: '#FFF' }]}>
                    <Text>Em promoção</Text>
                    <FlatList data={registrosPromocao} horizontal={true} renderItem={({ item, index }) => this.renderItem(item, index)} keyExtractor={(item) => item._id.toString()} onEndReached={onEndReachedPromocao} onEndReachedThreshold={0.5} ListFooterComponent={renderFooterPromocao} />
                </View>

                <View style={[styles.cardsContainer, { backgroundColor: '#F5F5F5' }]}>
                    <Text>Fantasia</Text>
                    <FlatList data={registrosFantasia} horizontal={true} renderItem={({ item, index }) => this.renderItem(item, index)} keyExtractor={(item) => item._id.toString()} onEndReached={onEndReachedFantasia} onEndReachedThreshold={0.5} ListFooterComponent={renderFooterFantasia} />
                </View>
            </View>
        </ScrollView>
    )
}

export default List