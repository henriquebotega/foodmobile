
import { Dimensions, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    cardsContainer: {
        height: 200,
        alignSelf: 'stretch',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    titulo: {
        alignSelf: 'center',
    },
    card: {
        borderWidth: 1,
        borderColor: '#DDD',
        borderRadius: 5,
        padding: 3,
        margin: 20,
        width: 100
    },
    foto: {
        width: '100%',
        height: '85%',
        resizeMode: 'cover'
    }
});

export default styles