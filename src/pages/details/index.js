import React, { useState, useEffect } from 'react';
import { Text, Image, Dimensions, ScrollView, View } from 'react-native';
import Reactotron from 'reactotron-react-native';

import styles from './styles';
import api from '../../services';

const DetailsProduct = ({ navigation }) => {

    const [loading, setLoading] = useState(true)
    const [registro, setRegistro] = useState({})

    useEffect(() => {
        const id = navigation.getParam('_id');

        api.get('/food/' + id).then((res) => {
            setRegistro(res.data)
            setLoading(false)
        })
    }, [])

    return (
        <View style={styles.viewHorizontal}>
            {loading && <Text>Aguarde...</Text>}

            {!loading &&
                <View>
                    <Text>Title: {registro.title}</Text>
                    <Text>Description: {registro.description}</Text>
                    <Text>Price: {registro.price}</Text>
                    <Text>Company: {registro._company.title}</Text>
                </View>
            }
        </View>
    )
}

export default DetailsProduct