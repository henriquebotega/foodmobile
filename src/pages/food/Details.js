import React, { useState, useEffect } from 'react';
import { Text, Image, Dimensions, ScrollView, View } from 'react-native';
import Reactotron from 'reactotron-react-native';

import styles from './styles';
import api from '../../services';

const Details = () => {

    const [totalRegistros, setTotalRegistros] = useState(0)
    const [registros, setRegistros] = useState([])
    const [pagina, setPagina] = useState(0)

    useEffect(() => {
        Reactotron.log(registros)
    }, [registros])

    useEffect(() => {
        api.get('/food').then((res) => {
            setRegistros(res.data.registros)
            setTotalRegistros(res.data.total)
        })
    }, [])

    moveScroll = (e) => {
        let contentOffset = e.nativeEvent.contentOffset;
        let viewSize = e.nativeEvent.layoutMeasurement;

        let pgAtual = Math.floor(contentOffset.x / viewSize.width);
        let total = (registros.length - 1);

        // Quando estiver na ultima página, buscará mais resultados
        if (total == pgAtual) {

            // Se houver mais registros, continua buscando por demanda (5)
            if ((pagina * registros.length) < totalRegistros) {
                setPagina(pagina + 1)

                api.get('/food?page=' + (pagina + 1)).then((res) => {
                    setRegistros([...registros, ...res.data.registros])
                })
            }
        }
    }

    const { width, height } = Dimensions.get('window')

    return (
        <ScrollView style={{ flex: 1 }} horizontal={true} pagingEnabled={true} showsHorizontalScrollIndicator={true} onMomentumScrollEnd={moveScroll}>
            {registros.map((obj, i) => {
                return (
                    <View key={i} style={styles.viewHorizontal}>
                        <Image style={styles.foto} source={{ uri: 'https://picsum.photos/id/' + i + '/300/300' }} />
                    </View>
                )
            })}
        </ScrollView>
    )
}

export default Details