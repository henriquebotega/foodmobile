import { Dimensions, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    viewHorizontal: {
        backgroundColor: '#5f9ea0',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: Dimensions.get('window').width,
    },
    foto: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        alignSelf: 'flex-start',
    }

});

export default styles