import Reactotron from 'reactotron-react-native'

const reactotron = Reactotron.configure({ host: '10.10.20.215', name: 'Food' }).useReactNative().connect();

export default reactotron
