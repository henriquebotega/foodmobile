import axios from 'axios';

const api = axios.create({
  // baseURL: 'https://foodbackend.herokuapp.com/api'
  baseURL: 'http://10.0.0.7:4000/api',
});

export default api;
