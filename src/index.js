import React from 'react';
import Routes from './routes'

import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: Async Storage'])
YellowBox.ignoreWarnings(['Warning: componentWillReceiveProps'])

if (__DEV__) {
    import('./config').then(() => console.log('Reactotron Configured'))
}

const App = () => <Routes />
export default App